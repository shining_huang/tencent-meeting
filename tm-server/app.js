const express = require('express')
// 配置跨域对象
const cors = require('cors')
// 解析HTTP请求中的请求体，将其转换为JavaScript对象
const bodyParser = require('body-parser')
const authRouter = require('./routes/auth')
const userRouter = require('./routes/user')
const roomRouter = require('./routes/room')
const historyRouter = require('./routes/history')
// 引入查询数据库连接函数
const { db, executeQuery } = require('./utils/db')
// 创建 express 实例
const app = express()
// 创建信令端口
const server = require('http').createServer(app)
// 引入信令服务器
const initializeSocketServer = require('./socket/socketServer')
initializeSocketServer(server)

// 跨域中间件
app.use(cors())
// 查询数据库连接是否关闭的中间件
app.use((req, res, next) => {
  executeQuery(db) // 执行数据库查询
  next() // 继续处理下一个中间件或路由
})
// 使用body-parser中间件解析请求体数据
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// 将路由挂载到app上
app.use('/api', authRouter)
app.use('/api', userRouter)
app.use('/api', roomRouter)
app.use('/api', historyRouter)

server.listen(3001, () => {
  console.log('信令端口已启动')
})
app.listen(3000, () => {
  console.log('服务器已启动')
})
