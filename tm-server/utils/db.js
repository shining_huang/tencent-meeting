const mysql = require('mysql')

// 创建MySQL连接池
const db = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'tm-server',
  connectTimeout: 10000
})

// 执行数据库查询的函数
function executeQuery(db) {
  db.query('SELECT 1', (error, results) => {
    if (error) {
      console.log('数据库连接断开，尝试重新连接...')
      db.end() // 先关闭连接
      db.getConnection(error => {
        if (error) {
          console.log('重新连接失败:', error.message)
        } else {
          console.log('重新连接成功')
        }
      })
    }
  })
}

module.exports = {
  db,
  executeQuery
}
