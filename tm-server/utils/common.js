const {
  createRoomInfoTableSql,
  createRoomParticipantsTableSql,
  createRoomChatTableSql,
  createUpdateIsMeetingOnTriggerSql
} = require('../sql/roomGroup')

const {
  createUserHistorySql,
  updateUserInfoEndTimeSql
} = require('../sql/history')
async function createRoomTable(queryPromise, roomId, username) {
  // 创建房间人员表
  await queryPromise(createRoomParticipantsTableSql, [roomId])
  // 创建聊天记录表
  await queryPromise(createRoomChatTableSql, [roomId])
  // 创建房间信息表
  await queryPromise(createRoomInfoTableSql, [roomId, roomId, roomId])
  // 创建历史记录表
  await queryPromise(createUserHistorySql, [roomId])
  // 创建历史记录更新给信息表修改结束时间触发器
  await queryPromise(updateUserInfoEndTimeSql, [roomId, roomId, roomId])
  // 创建根据结束时间去修改状态的信息表触发器
  await queryPromise(createUpdateIsMeetingOnTriggerSql, [roomId, roomId])
}

// 生成roomId
function generateRandomRoomId() {
  const timestamp = Date.now().toString() // 获取当前时间戳
  const random = Math.floor(Math.random() * 900000) + 100000 // 生成一个六位随机数
  const roomId = (parseInt(timestamp) + random) % 1000000 // 取模运算确保生成六位数

  return roomId.toString().padStart(6, '0') // 将roomId转换为字符串，并在前面补零至六位
}

module.exports = {
  createRoomTable,
  generateRandomRoomId
}
