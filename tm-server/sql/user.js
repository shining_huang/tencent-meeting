/**
 * 专门用来存放用户操作相关
 */
const sqlStatements = {
  // 修改基本信息
  updateBaseInfo: `UPDATE userInfo SET nickname = ?, phone = ?, gender = ? WHERE id = ?`,
  // 修改密码
  updatePass: `UPDATE userInfo SET password = ? WHERE id = ?`,
  // 修改头像
  updateAvatar: 'UPDATE userInfo SET avatar = ? WHERE id = ?',
}

module.exports = sqlStatements
