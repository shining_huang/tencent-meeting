/**
 * 房间管理相关的SQL语句集合。
 * @module sqlStatements
 */
const sqlStatements = {
  /**
   * 创建房间信息表的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换SQL语句中所有的“?”占位符，需要传递三次。
   */
  createRoomInfoTableSql: `
    CREATE TABLE IF NOT EXISTS room_?_info (
      roomId INT,
      account VARCHAR(255),
      title VARCHAR(255),
      capacity INT,
      timeMax INT,
      password VARCHAR(255),
      startTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      endTime VARCHAR(255),
      isMeetingOn VARCHAR(255),
      MID INT PRIMARY KEY AUTO_INCREMENT
    )
  `,

  /**
   * 根据isMeetingOn的状态去查询现在是哪一个会议
   */
  selectmIdByIsMeetingOnSql: `
  SELECT * FROM room_?_info WHERE isMeetingOn = 'In Progress'`,

  /**
   * 修改房间信息中的数据
   */
  generateUpdateRoomInfoTableSql: (roomId, fieldMappings) => {
    const updates = Object.entries(fieldMappings).map(([key, value]) => {
      return `${key} = ?`
    })

    return `
      UPDATE room_${roomId}_info
        SET ${updates.join(', ')}
      WHERE roomId = ?
    `
  },
  /**
   * 向房间信息表中插入数据的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   * @param {number} roomId - 房间ID，将替换第二个“?”占位符。
   * @param {string} account - 房主账号，将替换第三个“?”占位符。
   * @param {string} title - 房间标题，将替换第四个“?”占位符。
   * @param {number} capacity - 房间人数限制，将替换第五个“?”占位符。
   * @param {number} timeMax - 最长时间限制，将替换第六个“?”占位符。
   * @param {string} password - 房间密码，将替换第七个“?”占位符。
   * @param {string} isMeetingOn - 会议是否正在进行，将替换第八个“?”占位符。
   */
  insertRoomInfoTableSql: `
    INSERT INTO room_?_info (roomId, account, title, capacity, timeMax, password,isMeetingOn) VALUES (?, ?, ?, ?, ?, ?, ?)
    `,

  /**
   * 查询房间信息表中的信息
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   */
  selectRoomInfoTableMaxSql: `SELECT * FROM room_?_info`,

  /**
   * 创建房间参与者表的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换SQL语句中所有的“?”占位符，需要传递一次。
   */
  createRoomParticipantsTableSql: `
    CREATE TABLE IF NOT EXISTS room_?_participants (
      id INT PRIMARY KEY AUTO_INCREMENT,
      roomId INT,
      userId VARCHAR(255) NOT NULL,
      account VARCHAR(255) NOT NULL,
      avatar MEDIUMTEXT,
      enterTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )
  `,
  /**
   * 向房间参与者表中插入数据的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   * @param {number} roomId - 房间ID，将替换第二个“?”占位符。
   * @param {string} userId - 用户ID，将替换第三个“?”占位符。
   * @param {string} account - 用户账号，将替换第四个“?”占位符。
   * @param {string} avatar - 用户头像URL，将替换第五个“?”占位符。
   */
  insertRoomParticipantsTableSql: `INSERT INTO room_?_participants (roomId, userId, account, avatar) VALUES (?, ?, ?, ?)`,

  /**
   * 清空房间参与者表的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   */
  deleteAllRoomParticipantsSql: `TRUNCATE TABLE room_?_participants`,

  /**
   * 查询房间内每个参与者的信息的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   */
  selectRoomParticipantsSql: `SELECT * FROM room_?_participants`,

  /**
   * 从房间参与者表中删除数据的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   * @param {string} userId - 用户ID，将替换第二个“?”占位符。
   * @param {string} account - 用户账号，将替换第三个“?”占位符。
   */
  deleteRoomParticipantsTableSql: `DELETE FROM room_?_participants WHERE userId = ? OR account = ?`,
  /**
   * 创建聊天记录表的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换SQL语句中所有的“?”占位符，需要传递一次。
   */
  createRoomChatTableSql: `
    CREATE TABLE IF NOT EXISTS room_?_chat (
      id INT PRIMARY KEY AUTO_INCREMENT,
      roomId INT,
      userId VARCHAR(255) NOT NULL,
      account VARCHAR(255) NOT NULL,
      content MEDIUMTEXT NOT NULL,
      messageType VARCHAR(255) NOT NULL,
      recipientType VARCHAR(255) NOT NULL,
      sendTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )
  `,
  /**
   * 向聊天记录表中插入数据的SQL语句。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   * @param {number} roomId - 房间ID，将替换第二个“?”占位符。
   * @param {string} userId - 用户ID，将替换第三个“?”占位符。
   * @param {string} account - 账户名，将替换第四个“?”占位符。
   * @param {string} content - 聊天内容，将替换第五个“?”占位符。
   * @param {string} messageType - 消息类型，将替换第六个“?”占位符。
   * @param {string} recipientType - 接收者类型，将替换第七个“?”占位符。
   */
  insertRoomChatTableSql: `INSERT INTO room_?_chat (roomId, userId, account ,content, messageType, recipientType) VALUES (?, ?, ?,?, ?, ?)`,

  /**
   * 查询房间内聊天信息记录。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   */
  selectRoomChatSql: `SELECT * FROM room_?_chat`,

  /**
   * 查询房间内聊天信息的最后一条记录记录。
   * @param {number} roomNumber - 房间编号，将替换第一个“?”占位符。
   */
  selectRoomChatLastSql: `SELECT * FROM room_?_chat ORDER BY id DESC LIMIT 1`,
  /**
   * 查询在时间段内的聊天记录
   * @param roomNumber - 房间编号，将替换第一个“?”占位符。
   * @param startTime - 开始时间，将替换第二个“?”占位符。
   * @param endTime - 结束时间，将替换第三个“?”占位符。
   */
  selectRoomChatByTimeSql: `SELECT * FROM room_?_chat WHERE sendTime >= ? AND sendTime <= ?`,

  /**
   * 查询用户等级权限
   */
  selectRoleSql: `SELECT * FROM role WHERE user_role = ?`,
  /**
   * 根据结束时间去修改状态的info触发器
   */
  createUpdateIsMeetingOnTriggerSql: `
    CREATE TRIGGER update_?_info_isMeetingOn
    BEFORE UPDATE ON room_?_info
    FOR EACH ROW
    BEGIN
      IF OLD.endTime IS NULL AND NEW.endTime IS NOT NULL THEN
        SET NEW.isMeetingOn = 'Completed';
      END IF;
    END;
  `
}

module.exports = sqlStatements
