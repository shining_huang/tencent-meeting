/**
 * 专门用来存放登录注册相关的 sql 语句
 */
const sqlStatements = {
  // 查询用户名是否重复
  selectUsername: `SELECT COUNT(*) as count FROM userInfo WHERE username = ?`,
  // 创建用户
  insertUser: `INSERT INTO userInfo (username, password, phone, gender, roomId) VALUES (?, ?, ?, ?, ?)`,
  // 根据用户名或者id查询用户
  selectUser: `SELECT * FROM userInfo WHERE username = ? OR id = ?`
}

module.exports = sqlStatements
