/**
 * 专门用来处理用户相关的接口
 */
const express = require('express')
const util = require('util')
const userRouter = express.Router()
const jwt = require('../utils/jwt')

// 引入 db 文件
const { db } = require('../utils/db')
const { selectUser } = require('../sql/auth')
const { updateBaseInfo, updatePass, updateAvatar } = require('../sql/user')

// 将db.query转换为返回Promise的函数
const queryPromise = util.promisify(db.query).bind(db)

userRouter.get('/user/getUserInfo', async (req, res) => {
  try {
    // 获取token
    const token = req.headers.authorization.split(' ')[1]
    if (token === undefined) {
      res.status(404).json({ error: 'token不存在' })
    }

    // 验证token
    const result = await jwt.verifyToken(token)

    if (result.code === 404) {
      res.status(404).json({ error: result.msg })
    } else {
      // 解构出用户名
      const { username } = result
      // 查询用户信息
      const userInfo = await queryPromise(selectUser, [username, username])
      res.status(200).json({
        code: 200,
        msg: '获取信息成功',
        data: {
          userInfo: {
            ...userInfo[0]
          }
        }
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: '获取失败' })
  }
})

userRouter.put('/user/putUserInfo', async (req, res) => {
  try {
    // 获取传递过来的参数对象
    const { id, nickname, phone, gender } = req.body
    await queryPromise(updateBaseInfo, [nickname, phone, gender, id])
    res.status(200).json({
      code: 200,
      msg: '成功修改信息',
      data: {}
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: '修改失败' })
  }
})

userRouter.put('/user/putPass', async (req, res) => {
  try {
    // 获取传递过来的参数
    const { id, oldPass, newPass } = req.body
    //查询用户
    const userResult = await queryPromise(selectUser, [id, id])
    // 查询旧密码是否一致
    if (oldPass !== userResult[0].password)
      return res.status(401).json({ error: '密码不正确' })

    // 修改密码
    await queryPromise(updatePass, [newPass, id])
    res.status(200).json({
      code: 200,
      msg: '成功修改密码',
      data: {}
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: '修改失败' })
  }
})

userRouter.put('/user/putAvatar', async (req, res) => {
  try {
    // 获取传递过来的参数
    const { id, url } = req.body
    // 修改头像
    await queryPromise(updateAvatar, [url, id])
    res.status(200).json({
      code: 200,
      msg: '成功修改头像',
      data: {}
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: '修改失败' })
  }
})
module.exports = userRouter
