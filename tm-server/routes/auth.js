/**
 * 专门处理登录和注册的代码
 */
const express = require('express')
const util = require('util')
const authRouter = express.Router()
// 引入生成 token和 key 文件
const jwt = require('../utils/jwt')

// 引入 db 文件
const { db } = require('../utils/db')

// 将db.query转换为返回Promise的函数
const queryPromise = util.promisify(db.query).bind(db)

// 引入 sql 语句
const { insertUser, selectUsername, selectUser } = require('../sql/auth')

const { generateRandomRoomId, createRoomTable } = require('../utils/common')
// 处理注册请求
authRouter.post('/register', async (req, res) => {
  try {
    // 获取传递过来的参数 ['用户名','性别','联系方式','密码']
    const { username, gender, phone, password } = req.body
    // 判断是否缺少必需的参数
    if (!username || !gender || !phone || !password) {
      return res.status(400).json({ error: '缺少必需的参数' })
    }

    // 检查用户名是否重复
    const countResult = await queryPromise(selectUsername, [username])
    const count = countResult[0].count
    if (count > 0) {
      return res.status(409).json({ error: '用户名重复' })
    }
    const roomId = generateRandomRoomId()
    // 向数据库插入数据
    await queryPromise(insertUser, [username, password, phone, gender, roomId])
    // 创建用户所对应的房间id相关的表
    await createRoomTable(queryPromise, parseInt(roomId), username)
    res.status(200).json({
      code: 200,
      msg: '成功创建用户',
      data: {}
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: '注册失败' })
  }
})

// 登录接口
authRouter.post('/login', async (req, res) => {
  try {
    // 获取传递过来的参数
    const { username, password } = req.body

    // 检查是否缺失必要的参数
    if (!username || !password) {
      return res.status(400).json({ error: '缺少必需的参数' })
    }

    //查询用户
    const userResult = await queryPromise(selectUser, [username, username])

    // 判断用户是否存在
    if (userResult.length === 0)
      return res.status(404).json({ error: '用户不存在' })

    // 判断密码是否正确
    if (password !== userResult[0].password)
      return res.status(401).json({ error: '密码不正确' })

    // 生成密钥和token
    const secretKey = 'secretKey'
    const token = jwt.generateToken(username, secretKey)

    res.status(200).json({
      code: 200,
      msg: '登录成功',
      data: {
        userInfo: {
          ...userResult[0]
        },
        access_token: 'Bearer ' + token
      }
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: '登录失败' })
  }
})

module.exports = authRouter
