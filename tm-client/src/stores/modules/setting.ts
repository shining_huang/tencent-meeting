import { defineStore } from 'pinia'
import { reactive } from 'vue'
type SettingMeeting = {
  [key: string]: any
}
enum area {
  // 成员列表布局
  group_list = '0',
  // 三格列表布局
  three_list = '1'
}

// 设置模块
export const useSettingStore = defineStore(
  'setting',
  () => {
    /**
     * 会议设置相关
     */
    let settingMeeting = reactive<SettingMeeting>({
      // 是右侧成员列表布局还是三格布局
      areaStatus: area.group_list,
      // 右侧成员列表下的列表状态
      groupStatus: true,
      // 聊天界面的开关
      sendMsgStatus: true
    })

    // 改变聊天界面开关
    const changeSendMsgStatus = () => {
      settingMeeting.sendMsgStatus = !settingMeeting.sendMsgStatus
    }
    // 改变会议设置
    const setSettingMeeting = (key: keyof SettingMeeting, newVal: any) => {
      settingMeeting[key] = newVal
    }

    // 恢复默认
    const resetSettingMeeting = () => {
      setSettingMeeting('areaStatus', area.group_list)
      setSettingMeeting('groupStatus', true)
    }

    return {
      settingMeeting,
      setSettingMeeting,
      resetSettingMeeting,
      changeSendMsgStatus
    }
  },
  {
    persist: true
  }
)

export default useSettingStore
