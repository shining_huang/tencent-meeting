import { defineStore } from 'pinia'
import { reactive, ref } from 'vue'
type MeetingStatus = {
  [key: string]: boolean
}
// 设置模块
export const useMettingStore = defineStore('metting', () => {
  /**
   * 视频和语音相关
   */
  let meetingStatus = reactive<MeetingStatus>({
    // 视频状态
    video: false,
    // 语音状态
    audio: false
  })

  // 改变会议设置
  const setMeetingStatus = (key: keyof MeetingStatus, newVal: boolean) => {
    meetingStatus[key] = newVal
  }

  // 恢复默认
  const resetMeetingStatus = () => {
    setMeetingStatus('video', false)
    setMeetingStatus('audio', false)
  }

  return {
    meetingStatus,
    setMeetingStatus,
    resetMeetingStatus
  }
})

export default useMettingStore
