export interface ResponseData {
  code: number
  msg: string
}

export interface UserInfo {
  userInfo: {
    id: number
    username: string
    password: string
    phone: string
    nickname: string
    avatar: string
    gender: string
    registration_time: string
    user_role: string
    roomId: string
  }
}
