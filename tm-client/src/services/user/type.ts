import { ResponseData, UserInfo } from '@/types/commonType'

export interface UserInfoResponseData extends ResponseData {
  data: UserInfo
}

/**
 * 修改用户信息
 * 传递参数类型
 */
export interface PutUserInfoParams {
  id: number
  nickname: string | ''
  phone: string
  gender: string
}

export interface PutUserInfoResponseData extends ResponseData {
  data: {}
}

/**
 * 修改用户密码
 * 传递参数类型
 */
export interface PutPassParams {
  id: number
  oldPass: string
  newPass: string
  checkPass: string
}

export interface PutPassResponseData extends ResponseData {
  data: {}
}

/**
 * 修改用户头像
 * 传递参数类型
 */

export interface PutAvatarParams {
  id: number
  url: string
}

export interface PutAvatarResponseData extends ResponseData {
  data: {}
}
