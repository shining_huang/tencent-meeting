import request from '@/utils/request'
import {
  UserInfoResponseData,
  PutUserInfoResponseData,
  PutUserInfoParams,
  PutPassParams,
  PutPassResponseData,
  PutAvatarParams,
  PutAvatarResponseData,
} from './type'

enum URL {
  // 获取用户信息路径
  GETUSERINFO_URL = '/user/getUserInfo',
  // 修改用户信息路径
  PUTUSERINFO_URL = '/user/putUserInfo',
  // 修改用户密码路径
  PUTPASS_URL = '/user/putPass',
  // 修改用户头像路径
  PUTDATEAVATAR_URL = '/user/putAvatar',
}

/**
 * 获取用户信息
 */
export const getUserInfoAPI = () => {
  return request.get<UserInfoResponseData, any>(URL.GETUSERINFO_URL)
}

/**
 * 修改用户信息
 * @param data id: 用户id nickname: 昵称 phone: 联系方式 gender: 昵称
 */
export const putUserInfoAPI = (data: PutUserInfoParams) => {
  return request.put<PutUserInfoResponseData, any>(URL.PUTUSERINFO_URL, {
    ...data,
  })
}

/**
 * 修改用户密码
 * @param data id: 用户id oldPass: 旧密码 newPass: 新密码 checkPass:确认密码
 */
export const putPassAPI = (data: PutPassParams) => {
  return request.put<PutPassResponseData, any>(URL.PUTPASS_URL, {
    ...data,
  })
}

/**
 * 修改用户头像
 * @param data id:用户id url:图片路径
 */
export const putAvatarAPI = (data: PutAvatarParams) => {
  return request.put<PutAvatarResponseData, any>(URL.PUTDATEAVATAR_URL, {
    ...data,
  })
}
