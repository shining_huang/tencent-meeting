import request from '@/utils/request'
import {
  RegisterParams,
  RegisterResponseData,
  LoginParams,
  LoginResponseData,
} from './type'
enum URL {
  REGISTER_URL = '/register',
  LOGIN_URL = '/login',
}

/**
 *  注册api
 * @param data '用户名','性别','联系方式','密码'
 */
export const postRegisterAPI = (data: RegisterParams) => {
  return request.post<RegisterResponseData, any>(URL.REGISTER_URL, {
    ...data,
  })
}

/**
 *  登录api
 * @param data '用户名','密码'
 */
export const postLoginAPI = (data: LoginParams) => {
  return request.post<LoginResponseData, any>(URL.LOGIN_URL, {
    ...data,
  })
}
