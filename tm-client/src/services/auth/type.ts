import { ResponseData } from '@/types/commonType'

/**
 * 注册接口
 * 参数类型
 */
export interface RegisterParams {
  username: string
  gender: string
  phone: string
  password: string
}

/**
 * 注册接口
 * 返回数据类型
 */
export interface RegisterResponseData extends ResponseData {
  data: {}
}

/**
 * 登录接口
 * 参数类型
 */
export interface LoginParams {
  username: string
  password: string
}

/**
 * 登录接口
 * data类型
 */
export interface LoginData {
  access_token: string
}

/**
 * 登录接口
 * 返回数据类型
 */
export interface LoginResponseData extends ResponseData {
  data: LoginData
}
