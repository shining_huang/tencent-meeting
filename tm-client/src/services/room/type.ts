import { ResponseData } from '@/types/commonType'

export interface ParticipantsItem {
  id: number
  roomId: number
  userId: string
  account: string
  avatar: null | string
  enterTime: string
}

export interface ParticipantsData {
  participants: ParticipantsItem[]
}

export interface ParticipantsResponseData extends ResponseData {
  data: ParticipantsData
}

export interface DeleteParticipantsResponseData extends ResponseData {
  data: {}
}

export interface Chat {
  id: number
  roomId: number
  userId: string
  account: string
  content: string
  messageType: string
  recipientType: { value: string }[]
}

export interface ChatParams {
  roomId: string | number
  startTime: string
  endTime: string
}

export type Chats = Chat[]

export interface GetChatLastResponseData extends ResponseData {
  data: {
    chats: Chats
  }
}

export interface RoomInfo {
  MID: number
  roomId: number
  account: string
  title: string | null
  capacity: number
  timeMax: number
  password: string | null
  endTime: string | null
  isMeetingOn: 'In Progress' | 'Completed'
  startTime: string
}

export type RoomInfos = RoomInfo[]

export interface GetRoomsResponseData extends ResponseData {
  data: {
    roomInfo: RoomInfos
  }
}

export interface PutRoomsInfoParams {
  roomId: string
  title?: string
  password?: string
  startTime?: string
  endTime?: string
  isMeetingOn?: boolean
}

export interface PutRoomsInfoResponseData extends ResponseData {
  data: {}
}

export interface insertRoomInfoParams {
  roomId: string
  account: string
  title: string
  capacity: number
  password?: string
}
