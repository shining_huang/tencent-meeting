import request from '@/utils/request'
import {
  ParticipantsResponseData,
  DeleteParticipantsResponseData,
  GetChatLastResponseData,
  GetRoomsResponseData,
  PutRoomsInfoResponseData,
  PutRoomsInfoParams,
  insertRoomInfoParams,
  ChatParams
} from './type'

enum URL {
  GETPARTICIPANTS_URL = '/room/getParticipants',
  DELETEPARTICIPANTS_URL = '/room/deleteParticipants',
  GETCHATLAST_URL = '/room/getChatLast',
  GETROOMCHATBYTIME = 'room/getRoomChatByTime',
  GETROOMINFO_URL = '/room/getRoomInfo',
  PUTROOMINFO_URL = '/room/putRoomInfo',
  INSERTROOMINFO_URL = '/room/createRoom'
}

/**
 * 获取房间用户信息
 * @param roomId 房间id
 */
export const getParticipantsAPI = (roomId: string) => {
  return request.get<ParticipantsResponseData, any>(
    `${URL.GETPARTICIPANTS_URL}?roomId=${roomId}`
  )
}

/**
 * 删除房间内某一个用户
 * @param roomId 房间id
 * @param account 用户名
 */
export const deleteParticipantsAPI = (roomId: string, account: string) => {
  return request.delete<DeleteParticipantsResponseData, any>(
    `${URL.DELETEPARTICIPANTS_URL}?roomId=${roomId}&account=${account}`
  )
}

/**
 * 获取最后一条聊天记录
 * @param roomId 房间id
 */
export const getChatLastAPI = (roomId: string) => {
  return request.get<GetChatLastResponseData, any>(
    `${URL.GETCHATLAST_URL}?roomId=${roomId}`
  )
}

/**
 * 根据时间段去获取聊天记录
 */
export const getRoomChatByTimeAPI = (data: ChatParams) => {
  return request.get<GetChatLastResponseData, any>(URL.GETROOMCHATBYTIME, {
    params: {
      ...data
    }
  })
}

/**
 * 获取房间信息
 * @param roomId 房间id
 */
export const getRoomInfoAPI = (roomId: string) => {
  return request.get<GetRoomsResponseData, any>(
    `${URL.GETROOMINFO_URL}?roomId=${roomId}`
  )
}

/**
 * 修改房间信息
 */
export const putRoomInfoAPI = (data: PutRoomsInfoParams) => {
  return request.put<PutRoomsInfoResponseData, any>(URL.PUTROOMINFO_URL, data)
}

/**
 * 创建房间记录
 * @param data
 * @returns
 */
export const insertRoomInfoAPI = (data: insertRoomInfoParams) => {
  return request.post<any, any>(URL.INSERTROOMINFO_URL, data)
}
