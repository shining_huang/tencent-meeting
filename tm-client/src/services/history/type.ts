import { ResponseData } from '@/types/commonType'

/**
 * 添加历史记录接口
 * 参数类型
 */
export interface HistoryAddParamsType {
  mRoomId: string
  roomId: string
  MID: number
  title: string | null
  type: string
  startTime: string
  endTime: string
  nearTime: string
  host: string
}

/**
 * 添加历史记录接口
 * 返回数据类型
 */
export interface HistoryAddResponseData extends ResponseData {
  data: {}
}

/**
 * 查询历史记录
 * 历史记录
 */
export interface HistoryItem {
  id: number
  roomId: number
  MID: number
  title: string
  type: string
  startTime: string
  nearTime: string
  endTime: string
  continuousTime: string
  host: string
}

export interface HistoryResponseData extends ResponseData {
  data: {
    historyList: HistoryItem[]
  }
}
