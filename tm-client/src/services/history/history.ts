import request from '@/utils/request'

import {
  HistoryAddParamsType,
  HistoryAddResponseData,
  HistoryResponseData
} from './type'

enum URL {
  POSTHISTORYADD_URL = '/history/add',
  GETHISTORYBYTYPE_URL = '/history/getAllByType',
  DELETEHISTORY_URL = '/history/deleteHistory'
}

/**
 * 添加历史记录
 * @param {HistoryAddParams} data 需要的参数类型
 */
export const postHistoryAddAPI = (data: HistoryAddParamsType) => {
  return request.post<HistoryAddResponseData, any>(URL.POSTHISTORYADD_URL, {
    ...data
  })
}

/**
 * 根据类别查询历史记录
 */
export const getHistoryByTypeAPI = (roomId: string, type: string) => {
  return request.get<HistoryResponseData, any>(URL.GETHISTORYBYTYPE_URL, {
    params: {
      roomId,
      type
    }
  })
}

/**
 * 删除历史记录
 */
export const deleteHistoryAPI = (data: { roomId: string; id: number[] }) => {
  return request.delete<any, any>(URL.DELETEHISTORY_URL, {
    data: {
      roomId: data.roomId,
      id: data.id
    }
  })
}
